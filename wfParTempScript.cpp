 

void wfParTempScript(Int_t enTemp, Int_t deTemp, Int_t peTemp, Int_t toTemp, Int_t riTemp, Int_t thTemp)
{
	string stemp;

	gROOT->LoadMacro("pileParOpt.cpp+");
	gROOT->ProcessLine("Short_t enP[1], deP[1], peP[1];");
	stemp = "enP[0] = " + to_string(enTemp) + ";";
	gROOT->ProcessLine(stemp.c_str());
	stemp = "deP[0] = " + to_string(deTemp) + ";";
	gROOT->ProcessLine(stemp.c_str());
	stemp = "peP[0] = " + to_string(peTemp) + ";";
	gROOT->ProcessLine(stemp.c_str());
	stemp = "PileParOpt opt0(1250.,6,1,enP,deP,peP);";
	gROOT->ProcessLine(stemp.c_str());
	stemp = "opt0.effSweep(" + to_string(thTemp) + ',' + to_string(thTemp) + ',' + to_string(riTemp) + ',' + to_string(riTemp) + ',' + to_string(toTemp)
					+ ',' + to_string(toTemp) + ");";
	gROOT->ProcessLine(stemp.c_str());
gROOT->ProcessLine("TH2I* histWFP = opt0.plotWFPar();");
gROOT->ProcessLine("histWFP->Draw(\"colz\");");
}
