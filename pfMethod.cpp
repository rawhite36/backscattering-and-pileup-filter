

#ifndef pfMethod_cpp
#define pfMethod_cpp

void PileParOpt::findPeaks()
{
  pks[0]=0; pks[1]=0;

  if(metPar==0)
    for(Short_t i=50; i<WFLEN; ++i)
    {
      if(pks[0]==0 && trap[i]>threshPar)
        pks[0]=1;

      if(pks[0]==1 && trap[i]<=threshPar)
        pks[0]=2;

      if(pks[0]==2 && trap[i]>threshPar)
      {
        pks[0]=3;
        pks[1]=1;
      }
    }
  else if(metPar==1)
    for(Short_t i=50; i<WFLEN; ++i)
    {
      if(pks[0]==0 && trap[i]>threshPar)
        pks[0]=1;

      if(pks[0]==1 && trap[i]<trap[i-1] && trap[i-1]<trap[i-2] && trap[i-2]<trap[i-3]
         && trap[i-3]<trap[i-4])
        pks[0]=2;

      if(pks[0]==2 && trap[i]>threshPar && trap[i-1]<trap[i] && trap[i-2]<trap[i-1]
         && trap[i-3]<trap[i-2] && trap[i-4]<trap[i-3] && trap[i-5]<trap[i-4]
         && trap[i-6]<trap[i-5] && trap[i-7]<trap[i-6] && trap[i-8]<trap[i-7])
      {
        pks[0]=3;
        pks[1]=1;
      }
    }
  else if(metPar==2)
    for(Short_t i=50; i<WFLEN; ++i)
    {
      if(pks[0]==0 && trap[i]>threshPar)
        pks[0]=1;

      if(pks[0]==1 && trap[i]<trap[i-1] && trap[i-1]<trap[i-2] && trap[i-2]<trap[i-3]
         && trap[i-3]<trap[i-4])
        pks[0]=2;

      if(pks[0]==2 && trap[i-1]<trap[i] && trap[i-2]<trap[i-1] && trap[i-3]<trap[i-2]
         && trap[i-4]<trap[i-3] && trap[i-5]<trap[i-4] && trap[i-6]<trap[i-5]
         && trap[i-7]<trap[i-6] && trap[i-8]<trap[i-7])
      {
        pks[0]=3;
        pks[1]=1;
      }
    }
	else if(metPar==3)
    for(Short_t i=50; i<WFLEN; ++i)
    {
      if(pks[0]==0 && trap[i]>threshPar)
        pks[0]=1;

      if(pks[0]==1 && trap[i]<trap[i-1] && trap[i-1]<trap[i-2] && trap[i-2]<trap[i-3])
        pks[0]=2;

      if(pks[0]==2 && trap[i]>threshPar && trap[i-1]<trap[i] && trap[i-2]<trap[i-1]
         && trap[i-3]<trap[i-2] && trap[i-4]<trap[i-3] && trap[i-5]<trap[i-4]
         && trap[i-6]<trap[i-5])
      {
        pks[0]=3;
        pks[1]=1;
      }
    }
	else if(metPar==4)
		for(Short_t i=50; i<WFLEN; ++i)
    {
      if(pks[0]==0 && trap[i]>threshPar)
        pks[0]=1;

      if(pks[0]==1 && trap[i]<trap[i-1] && trap[i-1]<trap[i-2])
        pks[0]=2;

      if(pks[0]==2 && trap[i]>threshPar && trap[i-1]<trap[i] && trap[i-2]<trap[i-1]
         && trap[i-3]<trap[i-2] && trap[i-4]<trap[i-3])
      {
        pks[0]=3;
        pks[1]=1;
      }
    }
	else if(metPar==5)
		for(Short_t i=50; i<WFLEN; ++i)
    {
      if(pks[0]==0 && trap[i]>threshPar)
        pks[0]=1;

      if(pks[0]==1 && trap[i]<trap[i-1] && trap[i-1]<trap[i-2])
        pks[0]=2;

      if(pks[0]==2 && trap[i]>threshPar && trap[i-1]<trap[i] && trap[i-2]<trap[i-1]
         && trap[i-3]<trap[i-2] && trap[i-4]<trap[i-3])
      {
				if(WFLEN>i+100)
				{
        	pks[0]=3;
					pks[1]=1;
					i+=100;
				}
				else
					i=WFLEN;
      }

      if(pks[0]==3 && trap[i]>threshPar)
        pks[0]=4;

      if(pks[0]==4 && trap[i]<trap[i-1] && trap[i-1]<trap[i-2])
        pks[0]=5;

      if(pks[0]==5 && trap[i]>threshPar && trap[i-1]<trap[i] && trap[i-2]<trap[i-1]
         && trap[i-3]<trap[i-2] && trap[i-4]<trap[i-3])
      {
        pks[1]=0;
				i=WFLEN;
      }
    }
	else if(metPar==6)
		for(Short_t i=50; i<WFLEN; ++i)
    {
      if(pks[0]==0 && trap[i]>threshPar)
        pks[0]=1;

      if(pks[0]==1 && trap[i]<trap[i-1] && trap[i-1]<trap[i-2])
        pks[0]=2;

      if(pks[0]==2 && trap[i]>threshPar && trap[i-1]<trap[i] && trap[i-2]<trap[i-1])
      {
        pks[1]=1;
				i=WFLEN-1;
      }
    }
	else if(metPar==7)
		for(Short_t i=50; i<WFLEN; ++i)
    {
      if(pks[0]==0 && trap[i]>threshPar)
        pks[0]=1;

      if(pks[0]==1 && trap[i]<trap[i-1] && trap[i-1]<trap[i-2])
        pks[0]=2;

      if(pks[0]==2 && trap[i]>threshPar && trap[i-1]<trap[i] && trap[i-2]<trap[i-1])
      {
				if(WFLEN>i+100)
				{
        	pks[0]=3;
					pks[1]=1;
					i+=100;
				}
				else
					i=WFLEN;
      }

      if(pks[0]==3 && trap[i]>threshPar)
      {
        pks[1]=0;
				i=WFLEN-1;
      }
    }
	else if(metPar==8)
		for(Short_t i=50; i<WFLEN; ++i)
    {
      if(pks[0]==0 && trap[i]>threshPar)
        pks[0]=1;

      if(pks[0]==1 && trap[i]<trap[i-1] && trap[i-1]<trap[i-2] && trap[i-2]<trap[i-3])
        pks[0]=2;

      if(pks[0]==2 && trap[i]>threshPar && trap[i-1]<trap[i] && trap[i-2]<trap[i-1]
				 && trap[i-3]<trap[i-2])
      {
        pks[1]=1;
				i=WFLEN-1;
      }
    }
	else
		for(Short_t i=50; i<WFLEN; ++i)
    {
      if(pks[0]==0 && trap[i]>threshPar)
        pks[0]=1;

      if(pks[0]==1 && trap[i]<trap[i-1])
        pks[0]=2;

      if(pks[0]==2 && trap[i]>threshPar && trap[i-1]<trap[i])
      {
        pks[1]=1;
				i=WFLEN-1;
      }
    }
}

#endif
