

#ifndef pileParOpt_cpp
#define pileParOpt_cpp

#include "inc/pileParOpt.hpp"
#include "pfMethod.cpp"


PileParOpt::PileParOpt(Double_t thP, Short_t riP, Short_t toP, Double_t faP, Short_t meP)
            : WFLEN(3500), WFNUM(10000)
{
  threshPar = thP;
  risePar = riP;
  topPar = toP;
  fallPar = faP;
  metPar = meP;

  cnt = new Int_t**[10];

  for(Short_t i=0; i<10; ++i)
  {
    cnt0[i] = 0;
    cnt[i] = new Int_t*[7];

    for(Short_t j=0; j<7; ++j)
    {
      cnt[i][j] = new Int_t[9];

      for(Short_t k=0; k<9; ++k)
        cnt[i][j][k] = 0;
    }
  }

  cntS = nullptr;

  data = new Short_t[WFLEN];
  trap = new Double_t[WFLEN];
}

//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------

PileParOpt::PileParOpt(Double_t faP, Short_t meP, Short_t num, Short_t* enP, Short_t* deP,
                       Short_t* peP) : WFLEN(3500), WFNUM(10000)
{
  fallPar = faP;
  metPar = meP;

  for(Short_t i=0; i<num; ++i)
  {
    enerPar.push_back(enP[2*i]);
    enerPar.push_back(enP[2*i+1]); //needs to be commented out for the wfParTempScript.cpp
    delPar.push_back(deP[i]);
    percPar.push_back(peP[i]);
  }

  cnt = new Int_t**[10];

  for(Short_t i=0; i<10; ++i)
  {
    cnt0[i] = 0;
    cnt[i] = new Int_t*[7];

    for(Short_t j=0; j<7; ++j)
    {
      cnt[i][j] = new Int_t[9];

      for(Short_t k=0; k<9; ++k)
        cnt[i][j][k] = 0;
    }
  }

  data = new Short_t[WFLEN];
  trap = new Double_t[WFLEN];
}

//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------

PileParOpt::~PileParOpt()
{
  for(Short_t i=0; i<10; ++i)
  {
    for(Short_t j=0; j<7; ++j)
      delete[] cnt[i][j];

    delete[] cnt[i];
  }

  delete[] cnt;

  if(cntS!=nullptr)
  {
    for(Short_t i=0; i<(thParLim[1]-thParLim[0])/5.+.5; ++i)
    {
      for(Short_t j=0; j<(riParLim[1]-riParLim[0])/2+1; ++j)
      {
        for(Short_t k=0; k<(toParLim[1]-toParLim[0])/2+1; ++k)
        {
          for(Short_t l=0; l<enerPar.size(); ++l)
            delete[] cntS[i][j][k][l];

          delete[] cntS[i][j][k];
        }

        delete[] cntS[i][j];
      }

      delete[] cntS[i];
    }

    delete[] cntS;
  }

  delete[] data;
  delete[] trap;

  for(Int_t i=0; i<gr.size(); ++i)
    delete gr[i];

  for(Int_t i=0; i<mGr.size(); ++i)
    delete mGr[i];

  //for(Int_t i=0; i<grS.size(); ++i)
    //delete grS[i];

  //for(Int_t i=0; i<can.size(); ++i)
    //delete can[i];
}

//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------

void PileParOpt::efficiency(Char_t testFlag)
{
  string fNameIn;

  for(Short_t i=0; i<10; ++i)
  {
    fNameIn = "/home/ryan/bulk/res_programs/res_data/sim_new_37adcStDev/"
							+ to_string(100*(i+1)) + "adc_gausR_" + to_string((Short_t)fallPar/250)
							+ "usF_randNS_varP_10K/noP.dat";

    ifstream fin(fNameIn,ios::binary);

    if(fin.is_open())
    {
      fin.seekg(8,fin.cur);

      while(fin.seekg(33,fin.cur) && fin.read((Char_t*)data,7000))
			{
				removeBase();
        cnt0[i] += pileCheck();
			}

      fin.close();
    }
    else
      cout << "\nCannot Open File: " << fNameIn << '\n';

    for(Short_t j=0; (testFlag==0) ? (j<7) : (j<1); ++j)
      for(Short_t k=0; k<9; ++k)
      {
        fNameIn = "/home/ryan/bulk/res_programs/res_data/sim_new_37adcStDev/"
									+ to_string(100*(i+1)) + "adc_gausR_" + to_string((Short_t)fallPar/250)
									+ "usF_randNS_varP_10K/" + to_string(20*(j+1)) + "nsD_"
									+ to_string(10*(k+1)) + "P.dat";

        fin.open(fNameIn,ios::binary);

        if(fin.is_open())
        {
          fin.seekg(8,fin.cur);

          while(fin.seekg(33,fin.cur) && fin.read((Char_t*)data,7000))
					{
						removeBase();
            cnt[i][j][k] += pileCheck();
					}

          fin.close();
        }
        else
          cout << "\nCannot Open File: " << fNameIn << '\n';
      }
  }
}

//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------

void PileParOpt::efficiency(Short_t iTh, Short_t jRi, Short_t kTo)
{
  string fNameIn[2];
  Short_t flag;

  for(Short_t i=0; i<enerPar.size(); ++i)
  {
    fNameIn[0] = "/home/ryan/bulk/res_programs/res_data/sim_david/"
							+ to_string(enerPar[i]) + "adc_gausR_" + to_string((Short_t)fallPar / 250)
							+ "usF_randNS_varP_10K/" + to_string(delPar[i/2]) + "nsD_"
							+ to_string(percPar[i/2]) + "P.dat";
		fNameIn[1] = "/home/ryan

    ifstream fin(fNameIn[0], ios::binary);

    if(fin.is_open())
    {
      fin.seekg(8,fin.cur);

      while(fin.seekg(33,fin.cur) && fin.read((Char_t*)data,7000))
			{
				removeBase();
        cntS[iTh][jRi][kTo][i][0] += pileCheck();
			}

      fin.close();
    }
    else
      cout << "\nCannot Open File: " << fNameIn[0] << '\n';

    flag = -1; //needs to be commented out for the wfParTempScript.cpp

    for(Short_t j=0; j<i; ++j)
      if(enerPar[i]==enerPar[j])
        flag = j;

    if(flag==-1)
    {
      fNameIn[0] = "/home/ryan/bulk/res_programs/res_data/sim_new_37adcStDev/"
								+ to_string(enerPar[i]) + "adc_gausR_" + to_string((Short_t)fallPar/250)
								+ "usF_randNS_varP_10K/noP.dat";

      fin.open(fNameIn[0],ios::binary);

      if(fin.is_open())
      {
        fin.seekg(8,fin.cur);

        while(fin.seekg(33,fin.cur) && fin.read((Char_t*)data,7000))
				{
					removeBase();
          cntS[iTh][jRi][kTo][i][1] += pileCheck();
				}
      }
      else
        cout << "\nCannot Open File: " << fNameIn[0] << '\n';
    }
    else
      cntS[iTh][jRi][kTo][i][1] = cntS[iTh][jRi][kTo][flag][1];
  }
}

//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------

void PileParOpt::effSweep(Double_t thPL0, Double_t thPL1, Short_t riPL0, Short_t riPL1,
                          Short_t toPL0, Short_t toPL1)
{
  thParLim[0] = thPL0;
  thParLim[1] = thPL1;
  riParLim[0] = riPL0;
  riParLim[1] = riPL1;
  toParLim[0] = toPL0;
  toParLim[1] = toPL1;

  cntS = new Int_t****[(Short_t)((thPL1-thPL0)/5.+1.5)];

  for(Short_t i=0; i<(thPL1-thPL0)/5.+.5; ++i)
  {
    cntS[i] = new Int_t***[(riPL1-riPL0)/2+1];

    for(Short_t j=0; j<(riPL1-riPL0)/2+1; ++j)
    {
      cntS[i][j] = new Int_t**[(toPL1-toPL0)/2+1];

      for(Short_t k=0; k<(toPL1-toPL0)/2+1; ++k)
      {
        cntS[i][j][k] = new Int_t*[enerPar.size()];

        for(Short_t l=0; l<enerPar.size(); ++l)
        {
          cntS[i][j][k][l] =  new Int_t[2];

          cntS[i][j][k][l][0] = 0;
          cntS[i][j][k][l][1] = 0;
        }
      }
    }
  }

  for(Double_t i=thPL0; i<thPL1+1.; i+=5.)
    for(Short_t j=riPL0; j<riPL1+1; j+=2)
      for(Short_t k=toPL0; k<toPL1+1; k+=2)
      {
        threshPar = i;
        risePar = j;
        topPar = k;

        efficiency((i-thPL0)/5.+.5,(j-riPL0)/2,(k-toPL0)/2);
      }
}

//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------

void PileParOpt::plot()
{
  string stemp[2];
  Double_t xyTemp[2][10];

  for(Short_t i=0; i<7; ++i)
  {
		stemp[0] = "canA" + to_string(i);
    can.push_back(new TCanvas(stemp[0].c_str()));

    stemp[0] = "mGr" + to_string(i);
    mGr.push_back(new TMultiGraph(stemp[0].c_str(),stemp[0].c_str()));

    for(Short_t j=0; j<9; ++j)
    {
      for(Short_t k=0; k<10; ++k)
      {
        xyTemp[0][k] = 100*(k+1);
        xyTemp[1][k] = (Double_t)cnt[k][i][j]/WFNUM;
      }

      gr.push_back(new TGraph(10,xyTemp[0],xyTemp[1]));

      stemp[0] = "gr" + to_string(j);
      stemp[1] = "        " + to_string(10*(j+1)) + "% Init. Dep.";
      gr.back()->SetNameTitle(stemp[0].c_str(),stemp[1].c_str());

      if(j<3)
        gr.back()->SetLineColor(j+2);

      if(j==3 || j==5)
        gr.back()->SetLineColor(6);

      if(j>5)
        gr.back()->SetLineColor((8-j)+2);

     gr.back()->SetLineWidth(2);

      if(j>4)
        gr.back()->SetLineStyle(10);

      gr.back()->SetMarkerStyle(21);
      gr.back()->SetMarkerSize(.5);
      gr.back()->SetFillStyle(0);

      mGr.back()->Add(gr.back());
    }

    mGr.back()->Draw("apl");

    stemp[0] = "Efficiency of Pile-Up Identification with " + to_string(20*(i+1))
           + " ns Delay (10K Events)";
    mGr.back()->SetTitle(stemp[0].c_str());
    mGr.back()->GetXaxis()->SetTitle("Energy (adc)");
    mGr.back()->GetYaxis()->SetTitle("Yield");
    mGr.back()->GetXaxis()->SetTitleOffset(1.2);
    mGr.back()->GetYaxis()->SetTitleOffset(1.4);
    can.back()->BuildLegend();
  }

  can.push_back(new TCanvas("canB"));

  for(Short_t i=0; i<10; ++i)
  {
    xyTemp[0][i] = 100*(i+1);
    xyTemp[1][i] = (Double_t)(WFNUM - cnt0[i])/WFNUM;
  }

  gr.push_back(new TGraph(10,xyTemp[0],xyTemp[1]));

  gr.back()->Draw("apl");

  stemp[0] = "Efficiency of Pile-Up Identification with No Pile-Up (10K Events)";
  gr.back()->SetTitle(stemp[0].c_str());
  gr.back()->GetXaxis()->SetTitle("Energy (adc)");
  gr.back()->GetYaxis()->SetTitle("Yield");
  gr.back()->GetXaxis()->SetTitleOffset(1.2);
  gr.back()->GetYaxis()->SetTitleOffset(1.4);
  gr.back()->SetLineWidth(2);
  gr.back()->SetMarkerStyle(21);
  gr.back()->SetMarkerSize(.5);
}

//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------

/*void PileParOpt::stats(Double_t thPL0, Double_t thPL1, Short_t riPL0, Short_t riPL1,
                          Short_t toPL0, Short_t toPL1)
{
  for(Double_t i=thPL0; i<thPL1+1.; i+=5.)
  {
    cout << "cnt0[" << (Short_t)((i-thPL0)/5.+.5) << "]: "
         << cnt0[(Short_t)((i-thPL0)/5.+.5)] << '\n';

    for(Short_t j=riPL0; j<riPL1+1; j+=2)
    {
      for(Short_t k=toPL0; k<toPL1+1; k+=2)
        cout << "  cnt[" << (Short_t)((i-thPL0)/5.+.5) << "][" << (j-riPL0)/2 << "][" 
             << (k-toPL0)/2 << "]: "
             << cnt[(Short_t)((i-thPL0)/5.+.5)][(j-riPL0)/2][(k-toPL0)/2)];

      cout << '\n';
    }

    cout << "\n\n";
  }

  for(Short_t i=13; i<14; ++i)
  {
    cout << "cnt0[" << i << "]: " << cnt0[i] << '\n';

    for(Short_t j=4; j<8; ++j)
    {
      for(Short_t k=0; k<9; ++k)
        cout << "  cnt[" << i << "][" << j << "][" << k << "]: " << cnt[i][j][k];

      cout << '\n';
    }

    cout << '\n';
  }
}*/

//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------

void PileParOpt::plotSweep()
{
  string stemp[2];
  vector<Short_t> enerParTemp;
  Char_t epFlag;
  Short_t epCnt[2] = {};
  Double_t** xyzTemp = new Double_t*[4];

  for(Char_t i=0; i<4; ++i)
    xyzTemp[i] = new Double_t[((riParLim[1]-riParLim[0])/2+1)*((toParLim[1]
                           -toParLim[0])/2+1)];

  gStyle->SetTitleFontSize(.028);
  gStyle->SetLabelSize(.022,"xyz");
  gStyle->SetTitleSize(.028,"xyz");

  for(Short_t i=0; i<enerPar.size(); ++i)
  {
    epFlag = 0;

    for(Short_t j=0; j<enerParTemp.size(); ++j)
      if(enerParTemp[j]==enerPar[i])
        epFlag = 1;

    if(epFlag==0)
    {
      ++epCnt[0];
      enerParTemp.push_back(enerPar[i]);
    }
  }

  enerParTemp.clear();

  for(Short_t i=0; i<(thParLim[1]-thParLim[0])/5.+.5; ++i)
  {
    epCnt[1] = 0;

    for(Short_t j=0; j<enerPar.size(); ++j)
    {
      for(Int_t k=0; k<((riParLim[1]-riParLim[0])/2+1)*((toParLim[1]-toParLim[0])/2+1);
          ++k)
      {
        xyzTemp[0][k] = 2*(k/((toParLim[1]-toParLim[0])/2+1))+riParLim[0];
        xyzTemp[1][k] = 2*(k%((toParLim[1]-toParLim[0])/2+1))+toParLim[0];
        xyzTemp[2][k] = (Double_t)cntS[i][k/((toParLim[1]-toParLim[0])/2+1)][k%((toParLim[1]
                        -toParLim[0])/2+1)][j][0]/WFNUM;
        xyzTemp[3][k] = (Double_t)(WFNUM - cntS[i][k/((toParLim[1]-toParLim[0])/2+1)]
                        [k%((toParLim[1]-toParLim[0])/2+1)][j][1])/WFNUM;
      }

      epFlag = 0;

      for(Short_t l=0; l<enerParTemp.size(); ++l)
        if(enerParTemp[l]==enerPar[j])
          epFlag = 1;

      if(j==0)
      {
				stemp[0] = "canC" + to_string(i);
        can.push_back(new TCanvas(stemp[0].c_str()));

        if(enerPar.size()+epCnt[0]<7)
          can.back()->Divide((enerPar.size()+epCnt[0])/2,2);
        else if(enerPar.size()%3==0)
          can.back()->Divide((enerPar.size()+epCnt[0])/3,3);
        else
          can.back()->Divide((enerPar.size()+epCnt[0])/3+1,3);
      }

      if(epFlag==0)
      {
        can.back()->cd(enerPar.size()+(++epCnt[1]));

        grS.push_back(new TGraph2D(((riParLim[1]-riParLim[0])/2+1)*((toParLim[1]
                      -toParLim[0])/2+1), xyzTemp[0], xyzTemp[1], xyzTemp[3]));

        stemp[0] = "grS" + to_string(i*enerPar.size()+j) + "NP";
        stemp[1] = "#splitline{Threshold: " + to_string((Short_t)(5.*i+thParLim[0]+.5)) + " adc, Energy: "
                  + to_string(enerPar[j]) + " adc}{No Pile-up}";
        grS.back()->SetNameTitle(stemp[0].c_str(),stemp[1].c_str());
        grS.back()->SetMarkerStyle(21);
        grS.back()->SetMarkerSize(.8);

        grS.back()->Draw("colz");

        grS.back()->GetHistogram()->SetXTitle("Rise Parameter (4 ns bins)");
        grS.back()->GetHistogram()->SetYTitle("Top Parameter (4 ns bins)");
        grS.back()->GetHistogram()->SetZTitle("Yield");
        grS.back()->GetHistogram()->SetTitleOffset(1.2, "x");
        grS.back()->GetHistogram()->SetTitleOffset(1.3, "y");
        grS.back()->GetHistogram()->SetTitleOffset(0.94, "z");

        enerParTemp.push_back(enerPar[j]);
      }

      can.back()->cd(j+1);

      grS.push_back(new TGraph2D(((riParLim[1]-riParLim[0])/2+1)*((toParLim[1]
                    -toParLim[0])/2+1), xyzTemp[0], xyzTemp[1], xyzTemp[2]));

      stemp[0] = "grS" + to_string(i*enerPar.size()+j);
      stemp[1] = "#splitline{Threshold: " + to_string((Short_t)(5.*i+thParLim[0]+.5)) + " adc, Energy: "
                  + to_string(enerPar[j]) + " adc}{Delay: " + to_string(delPar[j/2])
                  + " ns, Percent Initially Deposited: " + to_string(percPar[j/2]) + "%}";
      grS.back()->SetNameTitle(stemp[0].c_str(),stemp[1].c_str());
      grS.back()->SetMarkerStyle(21);
      grS.back()->SetMarkerSize(.8);

      grS.back()->Draw("colz");

      grS.back()->GetHistogram()->SetXTitle("Rise Parameter (4 ns bins)");
      grS.back()->GetHistogram()->SetYTitle("Top Parameter (4 ns bins)");
      grS.back()->GetHistogram()->SetZTitle("Yield");
      grS.back()->GetHistogram()->SetTitleOffset(1.2, "x");
      grS.back()->GetHistogram()->SetTitleOffset(1.3, "y");
      grS.back()->GetHistogram()->SetTitleOffset(0.94, "z");
    }

    enerParTemp.clear();
  }

  for(Char_t i=0; i<4; ++i)
    delete[] xyzTemp[i];

  delete[] xyzTemp;
}

//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------

void PileParOpt::findPars(Short_t num, Double_t step, string foutName = "") const
{
  Double_t*** optPars = new Double_t**[num];
  Int_t*** max = new Int_t**[num];

  for(Short_t i=0; i<num; ++i)
  {
    optPars[i] = new Double_t*[enerPar.size()];
    max[i] = new Int_t*[enerPar.size()];

    for(UChar_t j=0; j<enerPar.size(); ++j)
    {
      optPars[i][j] = new Double_t[3];
      max[i][j] = new Int_t[2];
    }
  }

  for(Short_t i=0; i<num; ++i)
    for(Short_t j=0; j<enerPar.size(); ++j)
    {
      max[i][j][0] = 0;
      max[i][j][1] = 0;
    }

  for(Short_t i=0; i<num; ++i)
    for(Short_t j=0; j<(thParLim[1]-thParLim[0])/5.+.5; ++j)
      for(Short_t k=0; k<(riParLim[1]-riParLim[0])/2+1; ++k)
        for(Short_t l=0; l<(toParLim[1]-toParLim[0])/2+1; ++l)
          for(Short_t m=0; m<enerPar.size(); ++m)
            if(max[i][m][0]<cntS[j][k][l][m][0] && WFNUM-cntS[j][k][l][m][1]>WFNUM-WFNUM*step*i-1)
            {
              optPars[i][m][0] = thParLim[0] + 5.*j;
              optPars[i][m][1] = riParLim[0] + 2.*k;
              optPars[i][m][2] = toParLim[0] + 2.*l;
              max[i][m][0] = cntS[j][k][l][m][0];
              max[i][m][1] = WFNUM - cntS[j][k][l][m][1];
            }
						else
						{
							optPars[i][m][0] = 0;
              optPars[i][m][1] = 0;
              optPars[i][m][2] = 0;
              max[i][m][0] = 0;
              max[i][m][1] = 0;
						}

  if(foutName!="")
  {
    ofstream fout(foutName);

    for(Short_t i=0; i<num; ++i)
    {
      fout << "\nNo-Pile Efficiency Limit: " << 1.-step*i << '\n';

      for(Short_t j=0; j<enerPar.size(); ++j)
        fout << "\nRise Par.: " << 4*optPars[i][j][1] << " ns, Top Par.: "
             << 4*optPars[i][j][2] << " ns, Threshold Par.: " << optPars[i][j][0] << " adc"
             << "\n  Energy: " << enerPar[j] << " keV"
             << "\n    Delay: " << delPar[j/2] << " ns, Perc. Init. Dep.: " << percPar[j/2]
             << "%, Yield: " << 100.*(Double_t)max[i][j][0]/WFNUM << '%'
             << "\n    No Pile-Up Yield: " << 100.*(Double_t)max[i][j][1]/WFNUM << "%\n";
    }
  }
  else
    for(Short_t i=0; i<num; ++i)
    {
      cout << "\nNo-Pile Efficiency Limit: " << 1.-step*i << '\n';

      for(Short_t j=0; j<enerPar.size(); ++j)
        cout << "\nRise Par.: " << 4*optPars[i][j][1] << " ns, Top Par.: "
             << 4*optPars[i][j][2] << " ns, Threshold Par.: " << optPars[i][j][0] << " adc"
             << "\n  Energy: " << enerPar[j] << " keV"
             << "\n    Delay: " << delPar[j/2] << " ns, Perc. Init. Dep.: " << percPar[j/2]
             << "%, Yield: " << 100.*(Double_t)max[i][j][0]/WFNUM << '%'
             << "\n    No Pile-Up Yield: " << 100.*(Double_t)max[i][j][1]/WFNUM << "%\n";
    }

  for(Short_t i=0; i<num; ++i)
  {
    for(Short_t j=0; j<enerPar.size(); ++j)
    {
      delete[] optPars[i][j];
      delete[] max[i][j];
    }

    delete[] optPars[i];
    delete[] max[i];
  }

  delete[] optPars;
  delete[] max;
}

//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------

/*void PileParOpt::reset(Double_t thP, Short_t riP, Short_t toP, Double_t faP, Short_t meP)
{
  threshPar = thP;
  risePar = riP;
  topPar = toP;
  fallPar = faP;
  metPar = meP;

  for(Short_t i=0; i<32; ++i)
  {
    cnt0[i] = 0;

    for(Short_t j=0; j<9; ++j)
      for(Short_t k=0; k<9; ++k)
        cnt[i][j][k] = 0;
  }
}*/

//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------

void PileParOpt::removeBase()
{
	Int_t baseAvg = 0;

	for(Short_t i=50; i<900; ++i)
		baseAvg += data[i];

	baseAvg /= 850;

	for(Short_t i=0; i<WFLEN; ++i)
		data[i] -= baseAvg;
}

//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------

Char_t PileParOpt::pileCheck()
{
  Double_t s = 0.;
  Int_t p = 0, d = 0, trapLen = 2*risePar + topPar + 1;
  Short_t filt[trapLen];

  for(Short_t i=0; i<trapLen; ++i)
    filt[i] = 0;

  for(Short_t i=0; i<WFLEN; ++i)
  {
    filt[i%trapLen] = data[i];

    d = filt[i%trapLen] - filt[(i+trapLen-risePar)%trapLen]
        - filt[(i+trapLen-risePar-topPar)%trapLen] + filt[(i+1)%trapLen];
    p += d;
    s += p + fallPar*d;

    trap[i] = s / (risePar*fallPar);
  }

  findPeaks();
	findWFPar();

  if(pks[1]>0)
    return 1;
  else
    return 0;
}

//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------

void PileParOpt::findWFPar()
{
	Char_t pkFlag[2] = {};
	Short_t delFlag[3] = {};
	Short_t maxTemp[2] = {};
	Short_t delTemp[2] = {};

	for(Short_t i=50; i<WFLEN; ++i)
	{
    if(pkFlag[0]==0 && trap[i]>threshPar)
		{
      pkFlag[0]=1;
			delFlag[0]=i;
		}

    if(pkFlag[0]==1 && trap[i]<trap[i-1] && trap[i-1]<trap[i-2])
		{
      pkFlag[0]=2;
			delFlag[1]=i;
		}

    if(pkFlag[0]==2 && trap[i]>threshPar && trap[i-1]<trap[i] && trap[i-2]<trap[i-1])
    {
      pkFlag[1]=1;
			delFlag[2]=i;
			i=WFLEN-1;
    }
	}

	if(pkFlag[1]!=0)
	{
		for(Short_t i=delFlag[0]; i<delFlag[1]; ++i)
			if(trap[i]>maxTemp[0])
			{
				maxTemp[0]=trap[i];
				delTemp[0]=i;
			}

		for(Short_t i=delFlag[2]-2; i<delFlag[2]+2*risePar+topPar; ++i)
			if(trap[i]>maxTemp[1])
			{
				maxTemp[1]=trap[i];
				delTemp[1]=i;
			}
		tWFPar[0].push_back(maxTemp[0]+maxTemp[1]);
		tWFPar[1].push_back(delTemp[1]-delTemp[0]);
	}
	else
	{
		tWFPar[0].push_back(-1);
		tWFPar[1].push_back(-1);
	}
}

//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------

TH2I* PileParOpt::plotWFPar()
{
	TH2I* wfParHist = new TH2I("wfParH","WF Parameters",100,0,100,120,0,1200);

	for(Int_t i=0; i<tWFPar[0].size(); ++i)
		if(tWFPar[0][i]>-1)
			wfParHist->Fill(tWFPar[1][i],tWFPar[0][i]);

	return wfParHist;
}

#endif
