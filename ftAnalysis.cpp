

#ifndef ftAnalysis_cpp
#define ftAnalysis_cpp

class ftAnalysis
{
	Char_t exFlag;
	Double_t noPilePerc;
	Int_t method;
	Int_t rise;
	Int_t top;
	Int_t thresh;
	Char_t printFlag;
	Char_t extFlag;
	ifstream* fin;

	Char_t readData();
	Char_t readDataBlock();
	Char_t createPlots();

	public:
		const Short_t numEnerSteps = 10;
		const Short_t numDelSteps = 7;
  	Double_t* data[6];

		ftAnalysis(Double_t,Int_t,Int_t,Int_t,Int_t, Char_t, Char_t);
		~ftAnalysis();
		Char_t execute();
};

ftAnalysis::ftAnalysis(Double_t noPPerc, Int_t met, Int_t to, Int_t ri, Int_t th,
											 Char_t pFlag, Char_t eFlag)
{
	exFlag = 0;
	noPilePerc = noPPerc;
	method = met;
	top = to;
	rise = ri;
	thresh = th;
	printFlag = pFlag;
	extFlag = eFlag;

	if(extFlag==0)
	{
		data[0] = new Double_t[70]; // delay (x-axis)
		data[1] = new Double_t[70]; // energy (y-axis)
		data[2] = new Double_t[70]; // yield
		data[3] = new Double_t[70]; // rise par.
		data[4] = new Double_t[70]; // top par.
		data[5] = new Double_t[70]; // threshold par.

		for(Short_t i=0; i<70; ++i)
		{
		  data[0][i] = 20.*(i/10 + 1);
		  data[1][i] = 100.*(i%10 + 1);
		  data[2][i] = 0.;
		  data[3][i] = 0.;
		  data[4][i] = 0.;
		  data[5][i] = 0.;
		}
	}
	/*else
	{
		data[0] = new Double_t[126]; // delay (x-axis)
		data[1] = new Double_t[126]; // energy (y-axis)
		data[2] = new Double_t[126]; // yield
		data[3] = new Double_t[126]; // rise par.
		data[4] = new Double_t[126]; // top par.
		data[5] = new Double_t[126]; // threshold par.

		for(Short_t i=0; i<126; ++i)
		{
		  data[0][i] = 20.*(i/14 + 1);
		  data[1][i] = 400.*(i%14 + 1);
		  data[2][i] = 0.;
		  data[3][i] = 0.;
		  data[4][i] = 0.;
		  data[5][i] = 0.;
		}
	}*/
}

ftAnalysis::~ftAnalysis()
{
  for(Char_t i=0; i<6; ++i)
    delete[] data[i];
}

Char_t ftAnalysis::execute()
{
	if(exFlag==0)
	{
		readData();

		if(printFlag!=0)
			createPlots();
	}

	exFlag = 1;

	return 0;
}

Char_t ftAnalysis::readData()
{
	string stemp[2];
	Char_t ctemp = ' ';
	Double_t dtemp;

	fin = new ifstream;

	if(extFlag==0)
		stemp[0] = "/home/ryan/bulk/res_programs/45Ca_exp/pileup/testing2/results/init50fall5wf18ns37me"
							 + to_string(method) + "/1000by100adc140by20ns/to" + to_string(top) + "ri"
							 + to_string(rise) + "th" + to_string(thresh) + '/';
	/*else
		stemp[0] = "/home/ryan/bulk/res_programs/45Ca_exp/pileup/presentations/finalFilter/"
							 "5600adc180ns/me" + to_string(method) + "ri" + to_string(rise) + "to"
							 + to_string(top) + "th" + to_string(thresh) + "fall5wf18";*/

	if(extFlag==0)
		for(Char_t i=0; i<7; ++i)
			for(Char_t j=0; j<5; ++j)
			{
				stemp[1] = stemp[0] + "/del" + to_string(20*i+20) + "/ener" + to_string(200*j+100) + 'a'
									 + to_string(200*j+200) + "/resultFile.txt";

				fin->open(stemp[1]);

				if(fin->is_open())
				{
					getline(*fin,stemp[1],':');

					while(!fin->eof())
					{
						*fin >> dtemp;

						if(noPilePerc<dtemp+1e-6 && noPilePerc>dtemp-1e-6)
							readDataBlock();
						else
							for(Char_t i=0; i<11; ++i)
								getline(*fin,stemp[1]);

						getline(*fin,stemp[1],':');
					}

					fin->close();
				}
				else
				{
					cout << "\nCannot Open File: " << stemp[1] << "\n\n";
					fin->clear();
				}
			}
	/*else
		for(Char_t i=0; i<9; ++i)
			for(Char_t j=0; j<7; ++j)
			{
				stemp[1] = stemp[0] + "/del" + to_string(20*i+20) + "init50/ener"
									 + to_string(800*j+400) + 'a' + to_string(800*j+800) + "/th50t100.txt";

				fin->open(stemp[1]);

				if(fin->is_open())
				{
					getline(*fin,stemp[1],':');

					while(!fin->eof())
					{
						*fin >> dtemp;

						if(noPilePerc<dtemp+1e-6 && noPilePerc>dtemp-1e-6)
							readDataBlock();
						else
							for(Char_t i=0; i<11; ++i)
								getline(*fin,stemp[1]);

						getline(*fin,stemp[1],':');
					}

					fin->close();
				}
				else
				{
					cout << "\nCannot Open File: " << stemp[1] << "\n\n";
					fin->clear();
				}
			}*/

	delete fin;

	return 0;
}

Char_t ftAnalysis::readDataBlock()
{
	string stemp;
	Int_t itemp[5];
	Double_t dtemp;

	for(Char_t i=0; i<3; ++i)
	{
		getline(*fin,stemp,':');
		*fin >> itemp[i+2];
	}

	getline(*fin,stemp,':');
	*fin >> itemp[1];

	getline(*fin,stemp,':');
	*fin >> itemp[0];

	getline(*fin,stemp,':');
	getline(*fin,stemp,':');
	*fin >> dtemp;
	dtemp /= 100.;

	if(extFlag==0)
	{
		data[2][10*(itemp[0]/20 - 1)+itemp[1]/100 - 1] = dtemp;
		data[3][10*(itemp[0]/20 - 1)+itemp[1]/100 - 1] = itemp[2];
		data[4][10*(itemp[0]/20 - 1)+itemp[1]/100 - 1] = itemp[3];
		data[5][10*(itemp[0]/20 - 1)+itemp[1]/100 - 1] = itemp[4];
	}
	/*else
	{
		data[2][14*(itemp[0]/20 - 1)+itemp[1]/400 - 1] = dtemp;
		data[3][14*(itemp[0]/20 - 1)+itemp[1]/400 - 1] = itemp[2];
		data[4][14*(itemp[0]/20 - 1)+itemp[1]/400 - 1] = itemp[3];
		data[5][14*(itemp[0]/20 - 1)+itemp[1]/400 - 1] = itemp[4];
	}*/

	getline(*fin,stemp,':');
	*fin >> dtemp;
	dtemp /= 100.;

	if(noPilePerc>dtemp+1e-10)
	{
		if(extFlag==0)
		{
			data[2][10*(itemp[0]/20 - 1)+itemp[1]/100 - 1] = 0.;
			data[3][10*(itemp[0]/20 - 1)+itemp[1]/100 - 1] = 0;
			data[4][10*(itemp[0]/20 - 1)+itemp[1]/100 - 1] = 0;
			data[5][10*(itemp[0]/20 - 1)+itemp[1]/100 - 1] = 0;
		}
		/*else
		{
			data[2][14*(itemp[0]/20 - 1)+itemp[1]/400 - 1] = 0.;
			data[3][14*(itemp[0]/20 - 1)+itemp[1]/400 - 1] = 0;
			data[4][14*(itemp[0]/20 - 1)+itemp[1]/400 - 1] = 0;
			data[5][14*(itemp[0]/20 - 1)+itemp[1]/400 - 1] = 0;
		}*/
	}

	for(Char_t i=0; i<3; ++i)
	{
		getline(*fin,stemp,':');
		*fin >> itemp[i+2];
	}

	getline(*fin,stemp,':');
	*fin >> itemp[1];

	getline(*fin,stemp,':');
	*fin >> itemp[0];

	getline(*fin,stemp,':');
	getline(*fin,stemp,':');
	*fin >> dtemp;
	dtemp /= 100.;

	if(extFlag==0)
	{
		data[2][10*(itemp[0]/20 - 1)+itemp[1]/100 - 1] = dtemp;
		data[3][10*(itemp[0]/20 - 1)+itemp[1]/100 - 1] = itemp[2];
		data[4][10*(itemp[0]/20 - 1)+itemp[1]/100 - 1] = itemp[3];
		data[5][10*(itemp[0]/20 - 1)+itemp[1]/100 - 1] = itemp[4];
	}
	/*else
	{
		data[2][14*(itemp[0]/20 - 1)+itemp[1]/400 - 1] = dtemp;
		data[3][14*(itemp[0]/20 - 1)+itemp[1]/400 - 1] = itemp[2];
		data[4][14*(itemp[0]/20 - 1)+itemp[1]/400 - 1] = itemp[3];
		data[5][14*(itemp[0]/20 - 1)+itemp[1]/400 - 1] = itemp[4];
	}*/

	getline(*fin,stemp,':');
	*fin >> dtemp;
	dtemp /= 100.;

	if(noPilePerc>dtemp+1e-10)
	{
		if(extFlag==0)
		{
			data[2][10*(itemp[0]/20 - 1)+itemp[1]/100 - 1] = 0.;
			data[3][10*(itemp[0]/20 - 1)+itemp[1]/100 - 1] = 0;
			data[4][10*(itemp[0]/20 - 1)+itemp[1]/100 - 1] = 0;
			data[5][10*(itemp[0]/20 - 1)+itemp[1]/100 - 1] = 0;
		}
		/*else
		{
			data[2][14*(itemp[0]/20 - 1)+itemp[1]/400 - 1] = 0.;
			data[3][14*(itemp[0]/20 - 1)+itemp[1]/400 - 1] = 0;
			data[4][14*(itemp[0]/20 - 1)+itemp[1]/400 - 1] = 0;
			data[5][14*(itemp[0]/20 - 1)+itemp[1]/400 - 1] = 0;
		}*/
	}

	return 0;
}

Char_t ftAnalysis::createPlots()
{
  string stemp[2];
  TH2D* hist[4];
  TCanvas* can[4];

	gStyle->SetTitleFontSize(.028);
  gStyle->SetLabelSize(.022,"xyz");
  gStyle->SetTitleSize(.028,"xyz");
  gStyle->SetOptStat(0);
  gStyle->SetPalette(kRainBow);

  for(UChar_t i=0; i<1; ++i)
  {
    stemp[0] = "hist" + to_string(i);
    stemp[1] = "#splitline{" + to_string(4*rise) + " ns Rise, " + to_string(4*top)
							 + " ns Top, " + to_string(thresh) + " adc Threshold, Post-Filter Method "
							 + to_string(method) + "}{" + to_string((Int_t)(100.*noPilePerc))
							 + "% No-Pile-Up Efficiency, 50% Initial Deposit}";

		if(extFlag==0)
		{
    	hist[i] = new TH2D(stemp[0].c_str(), stemp[1].c_str(),7,10.,150.,10,50.,1050.);

		  for(Short_t j=0; j<70; ++j)
		      hist[i]->SetBinContent(j/10+1, j%10+1, data[i+2][j]);
		}
		/*else
		{
    	hist[i] = new TH2D(stemp[0].c_str(), stemp[1].c_str(),9,10.,190.,14,200.,5800.);

		  for(Short_t j=0; j<126; ++j)
		      hist[i]->SetBinContent(j/14+1, j%14+1, data[i+2][j]);
		}*/

    stemp[0] = "can" + to_string(i);
    can[i] = new TCanvas(stemp[0].c_str());

    hist[i]->Draw("colz");

    hist[i]->SetXTitle("Delay Between Pulses (ns)");
    hist[i]->SetYTitle("Total Energy Deposited (adc #approx 0.15 keV)");

		if(i==0)
			hist[i]->SetZTitle("Yield");
		else if(i==1)
			hist[i]->SetZTitle("Rise Par. (ns)");
		else if(i==2)
			hist[i]->SetZTitle("Top Par. (ns)");
		else if(i==3)
			hist[i]->SetZTitle("Threshold Par. (adc)");

    hist[i]->SetTitleOffset(1.8,"xy");
    hist[i]->SetTitleOffset(0.95,"z");

		if(printFlag>1)
		{
			if(extFlag==0)
				stemp[0] = "results/plots/1000by100adc140by20ns/"
									 + to_string(4*rise) + "ri" + to_string(4*top) + "to"
									 + to_string(thresh) + "th" + to_string(method) + "me"
									 + to_string((Int_t)(100.*noPilePerc)) + "p.gif";
			/*else
				stemp[0] = "../presentations/finalFilterPlots/single_filters/5600adc180ns/"
									 + to_string(4*rise) + "ri" + to_string(4*top) + "to"
									 + to_string(thresh) + "th" + to_string(method) + "me"
									 + to_string((Int_t)(100.*noPilePerc)) + "p.gif";*/

			can[i]->Print(stemp[0].c_str(),"gif");
		}
  }

	return 0;
}

#endif
