

void ftScript(string energy1, string energy2, string delay, string method, string top,
							 string rise, string thresh, string extFlag)
{
  time_t timer;
  time(&timer);

	string stemp;

	gROOT->LoadMacro("pileParOpt.cpp+");

	if(energy1=="0" && energy2=="0")
	{
		stemp = "PileParOpt opt0(" + thresh + ".," + rise + "," + top + ",1250.," + method
						+ ");";
		gROOT->ProcessLine(stemp.c_str());
		gROOT->ProcessLine("opt0.efficiency(0);");
		gROOT->ProcessLine("opt0.plot();");
	}
	else
	{
		gROOT->ProcessLine("Short_t enP[2], deP[1], peP[1];");
		stemp = "enP[0] = " + energy1 + ";";
		gROOT->ProcessLine(stemp.c_str());
		stemp = "enP[1] = " + energy2 + ";";
		gROOT->ProcessLine(stemp.c_str());
		stemp = "deP[0] = " + delay + ";";
		gROOT->ProcessLine(stemp.c_str());
		gROOT->ProcessLine("peP[0] = 50;");
		stemp = "PileParOpt opt0(1250.," + method + ",1,enP,deP,peP);";
		gROOT->ProcessLine(stemp.c_str());
		stemp = "opt0.effSweep(" + thresh + ',' + thresh + ',' + rise + ',' + rise + ',' + top
						+ ',' + top + ");";
		gROOT->ProcessLine(stemp.c_str());
		//gROOT->ProcessLine("opt0.plotSweep();");

		if(stoi(extFlag)==0)
			stemp = "opt0.findPars(11,.01,\"/home/ryan/bulk/res_programs/45Ca_exp/pileup/"
							"testing3/results/init50fall5wfdav19nois37me" + method
							+ "/to" + top + "ri" + rise + "th" + thresh + "/del"
							+ delay + "/ener" + energy1 + 'a' + energy2 + "/resultFile.txt\");";
		//else
			// use a folder other than 1000by100adc140by20ns

		gROOT->ProcessLine(stemp.c_str());
	}
	//else use effSweep with ranges for the sub-filter parameters

  cout << "\nTimer: " << difftime(time(nullptr),timer) << "\n\n";
}
