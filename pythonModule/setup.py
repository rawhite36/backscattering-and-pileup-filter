# This script will dynamically load the pile-up filter module.
# Intended for use with Python3

from distutils.core import setup, Extension

pFilterMod = Extension('pFilter',
                    sources = ['pileupFiltModule.cpp'])

setup (name = 'pFilterMod',
       version = '1.0',
       description = 'Pile-Up Filter Module',
			 author = 'Ryan A. Whitehead',
			 author_email = 'r.a.whiteh@gmail.com',
			 url = 'N/A',
			 long_description = '''
	       Pile-Up Filter for the Nab and 45Ca Experiments
   3 sub-filters are used, along with a post-filter identification algorithm
     sub-filter A: rise par. - 16 ns, top par. - 0 ns, threshold par. - 101 adc
     sub-filter B: rise par. - 36 ns, top par. - 0 ns, threshold par. - 90 adc
     sub-filter C: rise par. - 80 ns, top par. - 0 ns, threshold par. - 86 adc
   requires 2 arguments:
     an array of short integers (waveform)
			- this can be any iterable Python object (e.g. NumPy array)
     the decay time of the waveform
			- this should be a Python float type
   returns a Python tuple, with three entries, with responses to sub-filters A, B, and C
			- i.e. (A,B,C)
   requires that the baseline has already been subtracted from the waveform
   these sub-filter parameters have been optimized for pixel 64E of the 45Ca Experiment

   Ryan Whitehead, UTK, 09-21-18
''',
       ext_modules = [pFilterMod])
