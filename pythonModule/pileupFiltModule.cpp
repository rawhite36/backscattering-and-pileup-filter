/* Pile-Up Filter for the Nab and 45Ca Experiments
   3 sub-filters are used, along with a post-filter identification algorithm
     sub-filter A: rise par. - 16 ns, top par. - 0 ns, threshold par. - 101 adc
     sub-filter B: rise par. - 36 ns, top par. - 0 ns, threshold par. - 90 adc
     sub-filter C: rise par. - 80 ns, top par. - 0 ns, threshold par. - 86 adc
   requires 2 arguments:
     an array of short integers (waveform)
			- this can be any iterable Python object (e.g. NumPy array)
     the decay time of the waveform
			- this should be a Python float type
   returns a Python tuple, with three entries, with responses to sub-filters A, B, and C
			- i.e. (A,B,C)
   requires that the baseline has already been subtracted from the waveform
   these sub-filter parameters have been optimized for pixel 64E of the 45Ca Experiment

   Ryan Whitehead, UTK, 09-21-18 */


#include <Python.h>
#include<iostream>

// Pile-Up Filter Algorithm

static short* pileCheck(short*, double*, short, short, double, double);
static short* postFilter(double*, double);

static PyObject*
pFilter_execute(PyObject* self, PyObject* args)
{
	// Declare and Initialize Objects

	PyObject* pObj;
	PyObject* pIter;
	PyObject* pElem;
	const short wfLength = 3500;
	double fallPar;
  double* trap = new double[wfLength];
	short* data = new short[wfLength];
	short filtResponse[3][3];
	short* retArr[3];

	// Check and Convert Python Objects to C++ Objects

	if(!PyArg_ParseTuple(args, "Od", &pObj, &fallPar))
		return NULL;

	if(!(pIter=PyObject_GetIter(pObj)))
		return NULL;

	for(short i=0; (pElem=PyIter_Next(pIter)) && i<wfLength; ++i)
	{
		if(!PyLong_Check(pElem))
			return NULL;

		data[i] = PyLong_AS_LONG(pElem);
	}

	// Execute Sub-Filters

	retArr[0] = pileCheck(data, trap, 5, 0, 151, fallPar);
	retArr[1] = pileCheck(data, trap, 10, 0, 149, fallPar);
	retArr[2] = pileCheck(data, trap, 19, 0, 144, fallPar);

	for(char i=0; i<3; ++i)
	{
		for(char j=0; j<3; ++j)
			filtResponse[i][j] = retArr[i][j];

		delete[] retArr[i];
	}

	delete[] trap;
	delete[] data;

	// Return Filter Response

	return Py_BuildValue("iiiiiiiii", filtResponse[0][0], filtResponse[1][0], filtResponse[2][0],
											 filtResponse[0][1], filtResponse[1][1], filtResponse[2][1],
											 filtResponse[0][2], filtResponse[1][2], filtResponse[2][2]);
}

//----------------------------------------------------------------------------------------

static short* pileCheck(short* data, double* trap, short risePar, short topPar, double threshPar,
							 double fallPar)
{
	// Declare Initialize Objects

	const short wfLength = 3500;
  double s = 0.;
  int p = 0, d = 0, trapLen = 2*risePar + topPar + 1;
  short* filt = new short[trapLen];

  for(short i=0; i<trapLen; ++i)
    filt[i] = 0;

	// Implement Trapezoidal Sub-Filter

  for(short i=0; i<wfLength; ++i)
  {
    filt[i%trapLen] = data[i];

    d = filt[i%trapLen] - filt[(i+trapLen-risePar)%trapLen]
        - filt[(i+trapLen-risePar-topPar)%trapLen] + filt[(i+1)%trapLen];
    p += d;
    s += p + fallPar*d;

    trap[i] = s / (risePar*fallPar);
  }

	// Deallocate Memory

	delete[] filt;

	// Execute Post-Filter Method and Return It's Output

  return postFilter(trap, threshPar);
}

//----------------------------------------------------------------------------------------

static short* postFilter(double* trap, short risePar, short topPar, double threshPar)
{
	// Declare and Initialize Objects

	const short wfLength = 3500;
  char pks = 0;
	short delF[3] = {};
	short delT[2] = {};
	short maxT[2] = {};
	short* retArr = new short[3];
	retArr[0] = -1;
	retArr[1] = -1;
	retArr[2] = -1;

	// Implement Post-Filter Method

	for(short i=50; i<wfLength; ++i)
	{
		if(pks==0 && trap[i]>threshPar)
		{
			pks=1;
			delF[0]=i;
		}

		if(pks==1 && trap[i]<trap[i-1] && trap[i-1]<trap[i-2])
		{
			pks=2;
			delF[1]=i;
		}

		if(pks==2 && trap[i]>threshPar && trap[i-1]<trap[i] && trap[i-2]<trap[i-1])
		{
			retArr[0]=1;
			delF[2]=i;
			i=wfLength-1;
		}
	}

	if(retArr[0]==1)
	{
		for(short i=delF[0]; i<delF[1]; ++i)
			if(trap[i]>maxT[0])
			{
				maxT[0]=trap[i];
				delT[0]=i;
			}

		for(short i=delF[2]-2; i<delF[2]+2*risePar+topPar; ++i)
			if(trap[i]>maxT[1])
			{
				maxT[1]=trap[i];
				delT[1]=i;
			}

		retArr[1]=delT[1]-delT[0];
		retArr[2]=maxT[0]+maxT[1];
	}

	return retArr;
}

// Python Method Table

static PyMethodDef pFilterMethods[] = {
	{"execute",  pFilter_execute, METH_VARARGS,
	"Execute the Pile-Up Filter."},
	{NULL, NULL, 0, NULL}
};


static struct PyModuleDef pFilter =
{
    PyModuleDef_HEAD_INIT,
    "pFilter", /* name of module */
    NULL,          /* module documentation, may be NULL */
    -1,          /* size of per-interpreter state of the module, or -1 if the module keeps state in global variables. */
    pFilterMethods
};
// Initialization Function
PyMODINIT_FUNC
PyInit_pFilter(void)
{
    PyObject *module = PyModule_Create(&pFilter);
    return module;
}


