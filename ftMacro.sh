#!/bin/bash


if [ "$5" -eq 0 ]; then
	resultDir='/home/ryan/bulk/res_programs/45Ca_exp/pileup/testing3/results/init50fall5wfdav19nois37me'$1'/1000by100adc140by20ns/'

	if [ ! -d to$2ri$3th$4 ]; then
		cp -r $resultDir'.dirTemplate' $resultDir'to'$2'ri'$3'th'$4

		for i in {1..7}; do
			for j in {0..4}; do
				root -l -q 'ftScript.cpp("'$((200*j+100))'","'$((200*(j+1)))'","'$((20*i))'","'$1'","'$2'","'$3'","'$4'","'$5'")'
			done
		done
	fi
fi
#else
#	resultDir='/home/ryan/bulk/res_programs/45Ca_exp/pileup/presentations/finalFilter/5600adc180ns/'

#	cp -r $resultDir'.dirTemplate' $resultDir'me'$1'ri'$2'to'$3'th'$4'fall5wf18'

#	for i in {1..9}; do
#		for j in {0..6}; do
#			root -l -q 'filterTest.cpp("'$((400+800*j))'","'$((800*(j+1)))'","'$((20*i))'","'$1'","'$2'","'$3'","'$4'","'$5'")'
#		done
#	done
