
#include "ftAnalysis.cpp"

class autoScript
{
	TCanvas* can[4];
	TGraph* gr[8];

	public:
		autoScript(Int_t, Int_t);
		autoScript(Int_t);
		~autoScript();
};

autoScript::autoScript(Int_t threshMin, Int_t threshMax)
{
	Int_t top, rise, thresh;
	vector<ftAnalysis*> ftA;
	vector<Double_t> sum;
	Double_t sumMax;
	Int_t sumMaxCnt[2];
	UChar_t stripCheck[2];
	Double_t optThresh[2][22] = {};
	Int_t optThreshCnt[2][22];
	Int_t optEnerDel70[2][2][22] = {};

	for(UChar_t i=0; i<2; ++i)
		for(UChar_t j=0; j<22; ++j)
		{
			optThreshCnt[i][j] = -1;
			optEnerDel70[0][i][j] = -1;
			optEnerDel70[1][i][j] = -1;
		}

	for(UChar_t i=0; i<2; ++i)
	{
		top = i;

		for(UChar_t j=0; j<22; ++j)
		{
			rise = j+1;
			sumMax = 0.;
			sumMaxCnt[0] = -1;

			for(Short_t k=0; k<(threshMax-threshMin)/10+1; ++k)
			{
				thresh = threshMin + 10*k;

				ftA.push_back(new ftAnalysis(.99,6,top,rise,thresh,0,0));
				ftA[k]->execute();

				sum.push_back(0.);

				stripCheck[1] = 0;

				for(Short_t i=0; i<ftA[k]->numEnerSteps; ++i)
				{
					stripCheck[0] = 0;

					for(Short_t j=0; j<ftA[k]->numDelSteps; ++j)
					{
						if(ftA[k]->data[2][ftA[k]->numDelSteps*i+j]<1e-7)
							++stripCheck[0];

						sum[k] += ftA[k]->data[2][ftA[k]->numDelSteps*i+j];
					}

					if(stripCheck[0]==ftA[k]->numDelSteps && i>1)
						++stripCheck[1];
				}

				if(sum[k]>sumMax && stripCheck[1]==0)
				{
					sumMax = sum[k];
					sumMaxCnt[0] = k;
				}
			}

			if(sumMaxCnt[0]!=-1)
			{
				optThresh[i][j] = sumMax/(ftA[0]->numEnerSteps*ftA[0]->numDelSteps);
				optThreshCnt[i][j] = threshMin + 10*sumMaxCnt[0];

				//for(Short_t i=ftA[sumMaxCnt[0]]->numEnerSteps; i>0; --i)
					//for(Short_t j=ftA[sumMaxCnt[0]]->numDelSteps; j>0; --j)
						//if(ftA[sumMaxCnt[0]]->data[2][ftA[sumMaxCnt[0]]->numDelSteps*i+j]>.7)
						//{
							//optEnerDel70[0][i][j] = 100*i;

							//if(optEnerDel70[1][i][j]>20*j || optEnerDel70[1][i][j]==-1)
								//optEnerDel70[1][i][j] = 20*j;
						//}
			}

			for(Int_t i=0; i<ftA.size(); ++i)
				delete ftA[i];

			ftA.clear();
			sum.clear();

		}
	}

	Int_t xI[22];
	Double_t xD[22];

	for(UChar_t i=0; i<22; ++i)
	{
		xI[i] = i+1;
		xD[i] = i+1.;
	}

	can[0] = new TCanvas("can0");
	can[1] = new TCanvas("can1");
	can[2] = new TCanvas("can2");
	can[3] = new TCanvas("can3");

	for(UChar_t i=0; i<2; ++i)
	{
		gr[4*i] = new TGraph(22,xI,optThreshCnt[i]);
		gr[4*i+1] = new TGraph(22,xD,optThresh[i]);
		gr[4*i+2] = new TGraph(22,xI,optEnerDel70[0][i]);
		gr[4*i+3] = new TGraph(22,xI,optEnerDel70[1][i]);

		can[0]->cd();
		gr[4*i]->Draw(i==0 ? "" : "same");
		gr[4*i]->SetLineColor(i+1);

		can[1]->cd();
		gr[4*i+1]->Draw(i==0 ? "" : "same");
		gr[4*i+1]->SetLineColor(i+1);

		can[2]->cd();
		gr[4*i+2]->Draw(i==0 ? "" : "same");
		gr[4*i+2]->SetLineColor(i+1);

		can[3]->cd();
		gr[4*i+3]->Draw(i==0 ? "" : "same");
		gr[4*i+3]->SetLineColor(i+1);
	}
}

autoScript::autoScript(Int_t threshVal)
{
	Int_t top, rise, thresh;
	vector<ftAnalysis*> ftA;
	vector<Double_t> sum;
	Double_t sumMax;
	Int_t sumMaxCnt[2];
	UChar_t stripCheck[2];
	Double_t optThresh[2][22] = {};
	Int_t optThreshCnt[2][22];
	Int_t optEnerDel70[2][2][22] = {};

	for(UChar_t i=0; i<2; ++i)
		for(UChar_t j=0; j<22; ++j)
		{
			optThreshCnt[i][j] = -1;
			optEnerDel70[0][i][j] = -1;
			optEnerDel70[1][i][j] = -1;
		}

	for(UChar_t i=0; i<1; ++i)
	{
		top = i;

		for(UChar_t j=18; j<19; ++j)
		{
			rise = j+1;
			sumMax = 0.;
			sumMaxCnt[1] = -1;

			//if(sumMaxCnt[0]!=-1)
				for(Short_t k=0; k<19; ++k)
				{
					//thresh = threshMin + 10*sumMaxCnt[0] - 20 + k;
					thresh = threshVal - 9 + k;

					ftA.push_back(new ftAnalysis(.99,6,top,rise,thresh,0,0));
					ftA[k]->execute();

					sum.push_back(0.);

					stripCheck[1] = 0;

					for(Short_t i=0; i<ftA[k]->numEnerSteps; ++i)
					{
						stripCheck[0] = 0;

						for(Short_t j=0; j<ftA[k]->numDelSteps; ++j)
						{
							if(ftA[k]->data[2][ftA[k]->numDelSteps*i+j]<1e-7)
								++stripCheck[0];

							sum[k] += ftA[k]->data[2][ftA[k]->numDelSteps*i+j];
						}

						if(stripCheck[0]==ftA[k]->numDelSteps && i>1)
							++stripCheck[1];
					}

					if(sum[k]>sumMax && stripCheck[1]==0)
					{
						sumMax = sum[k];
						sumMaxCnt[1] = k;
					}
				}

			if(sumMaxCnt[1]!=-1)
			{
				optThresh[i][j] = sumMax/(ftA[0]->numEnerSteps*ftA[0]->numDelSteps);
				optThreshCnt[i][j] = threshVal - 9 + sumMaxCnt[1];

				cout << optThresh[i][j] << ", " << optThreshCnt[i][j] << '\n';
			}

			for(Int_t i=0; i<ftA.size(); ++i)
				delete ftA[i];

			ftA.clear();
			sum.clear();
		}
	}

	Int_t xI[22];
	Double_t xD[22];

	for(UChar_t i=0; i<22; ++i)
	{
		xI[i] = i+1;
		xD[i] = i+1.;
	}

	can[0] = new TCanvas("can0");
	can[1] = new TCanvas("can1");
	can[2] = new TCanvas("can2");
	can[3] = new TCanvas("can3");

	for(UChar_t i=0; i<2; ++i)
	{
		gr[4*i] = new TGraph(22,xI,optThreshCnt[i]);
		gr[4*i+1] = new TGraph(22,xD,optThresh[i]);
		gr[4*i+2] = new TGraph(22,xI,optEnerDel70[0][i]);
		gr[4*i+3] = new TGraph(22,xI,optEnerDel70[1][i]);

		can[0]->cd();
		gr[4*i]->Draw(i==0 ? "" : "same");
		gr[4*i]->SetLineColor(i+1);

		can[1]->cd();
		gr[4*i+1]->Draw(i==0 ? "" : "same");
		gr[4*i+1]->SetLineColor(i+1);

		can[2]->cd();
		gr[4*i+2]->Draw(i==0 ? "" : "same");
		gr[4*i+2]->SetLineColor(i+1);

		can[3]->cd();
		gr[4*i+3]->Draw(i==0 ? "" : "same");
		gr[4*i+3]->SetLineColor(i+1);
	}
}

autoScript::~autoScript()
{
	for(UChar_t i=0; i<8; ++i)
	{
		if(i<4)
			delete can[i];		

		delete gr[i];
	}
}
