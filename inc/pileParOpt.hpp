

#ifndef pileParOpt_hpp
#define pileParOpt_hpp

#include <iostream>
#include <fstream>
#include <vector>
#include <TCanvas.h>
#include <TMultiGraph.h>
#include <TGraph.h>
#include <TGraph2D.h>
#include <TH2.h>
#include <TStyle.h>

class PileParOpt
{
  const Short_t WFLEN; // 14 us
  const Short_t WFNUM;

  Double_t threshPar;
  Short_t risePar;
  Short_t topPar;
  Double_t fallPar;
  Short_t metPar;

  vector<Short_t> enerPar;
  vector<Short_t> delPar;
  vector<Short_t> percPar;

  Double_t thParLim[2];
  Short_t riParLim[2];
  Short_t toParLim[2];

  Int_t cnt0[30];
  Int_t*** cnt;

  Int_t***** cntS;

  Short_t* data;
  Double_t* trap;
  UChar_t pks[2];
	vector<Short_t> tWFPar[2];

  vector<TCanvas*> can;

  vector<TMultiGraph*> mGr;
  vector<TGraph*> gr;

  vector<TGraph2D*> grS;

	void removeBase();
  Char_t pileCheck();
  void findPeaks();
	void findWFPar();
  void efficiency(Short_t, Short_t, Short_t);

  public:
    PileParOpt(Double_t, Short_t, Short_t, Double_t, Short_t);
    PileParOpt(Double_t, Short_t, Short_t, Short_t*, Short_t*, Short_t*);
    ~PileParOpt();
    void efficiency(Char_t);
    void effSweep(Double_t, Double_t, Short_t, Short_t, Short_t, Short_t);
    void plot();
    //void stats() const;
    void plotSweep();
    //void expand();
    void findPars(Short_t, Double_t, string) const;
		TH2I* plotWFPar();
    //void reset(Double_t, Short_t, Short_t, Double_t, Short_t);
    //void clearPlots();
};

#endif
